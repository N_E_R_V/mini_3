#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <array>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <ctime>
#include <algorithm>

struct Point
{
    int x, y;
    Point() : Point(0, 0) {}
    Point(int x, int y) : x(x), y(y) {}
    bool operator==(const Point &rhs) const
    {
        return x == rhs.x && y == rhs.y;
    }
    bool operator!=(const Point &rhs) const
    {
        return !operator==(rhs);
    }
    Point operator+(const Point &rhs) const
    {
        return Point(x + rhs.x, y + rhs.y);
    }
    Point operator-(const Point &rhs) const
    {
        return Point(x - rhs.x, y - rhs.y);
    }
};

class OthelloBoard
{
public:
    enum SPOT_STATE
    {
        EMPTY = 0,
        BLACK = 1,
        WHITE = 2
    };
    static const int SIZE = 8;
    const std::array<Point, 8> directions{{Point(-1, -1), Point(-1, 0), Point(-1, 1),
                                           Point(0, -1), /*{0, 0}, */ Point(0, 1),
                                           Point(1, -1), Point(1, 0), Point(1, 1)}};
    std::array<std::array<int, SIZE>, SIZE> board;
    std::vector<Point> next_valid_spots;
    std::array<int, 3> disc_count;
    int cur_player;
    bool done;
    int winner;
    int player;

private:
    int get_next_player(int player) const
    {
        return 3 - player;
    }
    bool is_spot_on_board(Point p) const
    {
        return 0 <= p.x && p.x < SIZE && 0 <= p.y && p.y < SIZE;
    }
    int get_disc(Point p) const
    {
        return board[p.x][p.y];
    }
    void set_disc(Point p, int disc)
    {
        board[p.x][p.y] = disc;
    }
    bool is_disc_at(Point p, int disc) const
    {
        if (!is_spot_on_board(p))
            return false;
        if (get_disc(p) != disc)
            return false;
        return true;
    }
    bool is_spot_valid(Point center) const
    {
        if (get_disc(center) != EMPTY)
            return false;
        for (Point dir : directions)
        {
            // Move along the direction while testing.
            Point p = center + dir;
            if (!is_disc_at(p, get_next_player(cur_player)))
                continue;
            p = p + dir;
            while (is_spot_on_board(p) && get_disc(p) != EMPTY)
            {
                if (is_disc_at(p, cur_player))
                    return true;
                p = p + dir;
            }
        }
        return false;
    }
    void flip_discs(Point center)
    {
        for (Point dir : directions)
        {
            // Move along the direction while testing.
            Point p = center + dir;
            if (!is_disc_at(p, get_next_player(cur_player)))
                continue;
            std::vector<Point> discs({p});
            p = p + dir;
            while (is_spot_on_board(p) && get_disc(p) != EMPTY)
            {
                if (is_disc_at(p, cur_player))
                {
                    for (Point s : discs)
                    {
                        set_disc(s, cur_player);
                    }
                    disc_count[cur_player] += discs.size();
                    disc_count[get_next_player(cur_player)] -= discs.size();
                    break;
                }
                discs.push_back(p);
                p = p + dir;
            }
        }
    }

public:
    OthelloBoard()
    {
        reset();
    }
    void reset()
    {
        for (int i = 0; i < SIZE; i++)
        {
            for (int j = 0; j < SIZE; j++)
            {
                board[i][j] = EMPTY;
            }
        }
        board[3][4] = board[4][3] = BLACK;
        board[3][3] = board[4][4] = WHITE;
        cur_player = BLACK;
        disc_count[EMPTY] = 8 * 8 - 4;
        disc_count[BLACK] = 2;
        disc_count[WHITE] = 2;
        next_valid_spots = get_valid_spots();
        done = false;
        winner = -1;
    }
    std::vector<Point> get_valid_spots() const
    {
        std::vector<Point> valid_spots;
        for (int i = 0; i < SIZE; i++)
        {
            for (int j = 0; j < SIZE; j++)
            {
                Point p = Point(i, j);
                if (board[i][j] != EMPTY)
                    continue;
                if (is_spot_valid(p))
                    valid_spots.push_back(p);
            }
        }
        return valid_spots;
    }
    bool put_disc(Point p)
    {
        if (!is_spot_valid(p))
        {
            winner = get_next_player(cur_player);
            done = true;
            return false;
        }
        set_disc(p, cur_player);
        disc_count[cur_player]++;
        disc_count[EMPTY]--;
        flip_discs(p);
        // Give control to the other player.
        cur_player = get_next_player(cur_player);
        next_valid_spots = get_valid_spots();
        // Check Win
        if (next_valid_spots.size() == 0)
        {
            cur_player = get_next_player(cur_player);
            next_valid_spots = get_valid_spots();
            if (next_valid_spots.size() == 0)
            {
                // Game ends
                done = true;
                int white_discs = disc_count[WHITE];
                int black_discs = disc_count[BLACK];
                if (white_discs == black_discs)
                    winner = EMPTY;
                else if (black_discs > white_discs)
                    winner = BLACK;
                else
                    winner = WHITE;
            }
        }
        return true;
    }
    int Utility(int player)
    {
        int white_discs = disc_count[WHITE];
        int black_discs = disc_count[BLACK];
        if (player == BLACK)
            return black_discs - white_discs;
        else
            return white_discs - black_discs;
    }
    int Eval(int player)
    {
        int white_discs = disc_count[WHITE];
        int black_discs = disc_count[BLACK];
        int val;

        val = black_discs - white_discs;
        if (board[0][0] == WHITE)
            val--;
        else if (board[0][0] == BLACK)
            val++;

        if (board[1][1] == WHITE)
            val++;
        else if (board[1][1] == BLACK)
            val--;

        if (board[0][SIZE - 1] == WHITE)
            val--;
        else if (board[0][SIZE - 1] == BLACK)
            val++;

        if (board[1][SIZE - 2] == WHITE)
            val++;
        else if (board[1][SIZE - 1] == BLACK)
            val--;

        if (board[SIZE - 1][0] == WHITE)
            val--;
        else if (board[SIZE - 1][0] == BLACK)
            val++;

        if (board[SIZE - 2][1] == WHITE)
            val++;
        else if (board[SIZE - 1][0] == BLACK)
            val--;

        if (board[SIZE - 1][SIZE - 1] == WHITE)
            val--;
        else if (board[SIZE - 1][SIZE - 1] == BLACK)
            val++;

        if (board[SIZE - 2][SIZE - 2] == WHITE)
            val++;
        else if (board[SIZE - 2][SIZE - 2] == BLACK)
            val--;

        for (int i = 0; i < SIZE; i++)
        {
            if (board[0][i] == WHITE)
                val--;
            else if (board[0][i] == BLACK)
                val++;

            if (board[i][0] == WHITE)
                val--;
            else if (board[i][0] == BLACK)
                val++;

            if (board[SIZE - 1][i] == WHITE)
                val--;
            else if (board[SIZE - 1][i] == BLACK)
                val++;

            if (board[i][SIZE - 1] == WHITE)
                val--;
            else if (board[i][SIZE - 1] == BLACK)
                val++;
        }

        if (player == BLACK)
        {
            return val;
        }
        else
        {
            return -1 * val;
        }
    }
};

int player;
OthelloBoard game;
const int SIZE = 8;
std::array<std::array<int, SIZE>, SIZE> board;
std::vector<Point> next_valid_spots;

void read_board(std::ifstream &fin)
{
    fin >> player;
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            fin >> board[i][j];
            game.board[i][j] = board[i][j];
        }
    }
}

void read_valid_spots(std::ifstream &fin)
{
    int n_valid_spots;
    fin >> n_valid_spots;
    int x, y;
    for (int i = 0; i < n_valid_spots; i++)
    {
        fin >> x >> y;
        next_valid_spots.push_back({x, y});
    }
    game.next_valid_spots = next_valid_spots;
}

int DEPTH;

int negaMax(OthelloBoard cur_state, int depth, int alpha, int beta, std::ofstream &fout)
{
    if ((double)clock() / CLOCKS_PER_SEC == 10) // stop when time's up
        exit(0);

    if (cur_state.done)
    {
        return cur_state.Utility(cur_state.cur_player);
    }

    if (depth == 0)
    {
        return cur_state.Eval(cur_state.cur_player);
    }

    int result = -1 * SIZE * SIZE;

    for (size_t i = 0; i < cur_state.next_valid_spots.size(); i++)
    {
        OthelloBoard next_state = cur_state;
        Point p = cur_state.next_valid_spots[i];
        next_state.put_disc(p);

        int result_ = -1 * negaMax(next_state, depth - 1, -1 * beta, -1 * alpha, fout);

        if (result_ > result)
        {
            result = result_;

            if (depth == DEPTH)
            {
                fout << p.x << " " << p.y << std::endl;
                fout.flush();
            }
        }

        if (result >= beta)
            return result;

        alpha = std::max(alpha, result);
    }

    return result;
}

void write_valid_spot(std::ofstream &fout)
{
    srand(time(NULL));

    DEPTH = 2;
    negaMax(game, DEPTH, -1 * SIZE * SIZE, SIZE * SIZE, fout);

    DEPTH = 9;
    if (game.next_valid_spots.size() != 1)
        negaMax(game, DEPTH, -1 * SIZE * SIZE, SIZE * SIZE, fout);
    else
    {
        fout << next_valid_spots[0].x << " " << next_valid_spots[0].y << std::endl;
        fout.flush();
    }
}

int main(int, char **argv)
{
    std::ifstream fin(argv[1]);
    std::ofstream fout(argv[2]);
    read_board(fin);
    read_valid_spots(fin);
    write_valid_spot(fout);
    std::cout << (double)clock() / CLOCKS_PER_SEC << "S";
    fin.close();
    fout.close();
    return 0;
}
